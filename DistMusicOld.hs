import Network.Info
import Data.List.Index
import qualified Data.ByteString.Char8 as C
import Network.Socket hiding (recv)
import Network.Socket.ByteString (recv, sendAll)
import Control.Monad
import Control.Concurrent
import Data.List
import Data.Char (toLower)
import Data.List.Split
import System.Directory

printInterface :: (Int, String) -> IO()
printInterface (id, name) = do
  putStrLn ((show (id + 1)) ++ ") " ++ name)
  return ()
  
sendPacket :: String -> String -> String -> IO ()
sendPacket port message ip = do
  print ip
  addrinfos <- getAddrInfo Nothing (Just ip) (Just port)
  let serveraddr = head addrinfos
  sock <- socket (addrFamily serveraddr) Datagram defaultProtocol
  setSocketOption sock Broadcast 1
  connect sock (addrAddress serveraddr)
  sendAll sock $ C.pack (message)
  close sock
  return ()

makeIPAdress :: String -> String -> String
makeIPAdress pref post = pref ++ post
getIPAddresses :: String -> [String]
getIPAddresses prefix = do
  map (makeIPAdress prefix) nums
  where
  nums = map show [255..255]

toInt :: [Char] -> Int
lst (a, b) = b
toInt x = read x :: Int

discoverIP :: Int -> [(Int, IPv4)] -> String
discoverIP id ips = show (lst (ips !! id))
  
loop :: String -> String -> IO ()
loop broadcast local_ip = do
  putStr "> "
  command <- getLine
  let args = splitOn " " command
  let command_name = args !! 0
  case command_name of
    "/find" -> do
      let msg = local_ip ++ "%FIND%" ++ (args !! 1) 
      sendPacket "5555" msg broadcast
      loop broadcast local_ip
    _ -> do
      putStrLn("Incorrect command!")
      loop broadcast local_ip

main :: IO ()
main = do
  names_raw <- fmap (map name) getNetworkInterfaces
  let names = indexed(names_raw)
  ips_raw <- fmap (map ipv4) getNetworkInterfaces
  let ips = indexed(ips_raw)
  putStrLn("Select network interface:")
  names_raw <- fmap (map name) getNetworkInterfaces
  let names = indexed(names_raw)
  ips_raw <- fmap (map ipv4) getNetworkInterfaces
  let ips = indexed(ips_raw)
  mapM_ printInterface names
  putStr "Number: "
  num <- getLine
  let broadcast = "172.6.0.255"
  let local_ip = discoverIP ((toInt num) - 1) ips
  forkIO $ runUDPServer local_ip
  loop broadcast local_ip

  
{-runTCPServer :: IO ()
runTCPServer = withSocketsDo $ do
  sock <- listenOn $ PortNumber (toEnum 5556)
  sockHandler sock
  
sockHandlerTCP :: Socket -> IO ()
sockHandlerTCP sock = do
  (handle, _, _) <- accept sock
  hSetBuffering handle NoBuffering
  forkIO $ processTCPHandle handle
  sockHandlerTCP sock

processTCPHandle :: Handle -> IO ()
processTCPHandle handle = do
-}  
  
  
--runConn :: (Socket, SockAddr) -> IO ()
--runConn (sock, _) = do
--  Nothing
  
  
runUDPServer :: String -> IO ()
runUDPServer ip_address = do
  addrinfos <- getAddrInfo Nothing (Just "0.0.0.0") (Just "5555")
  let serveraddr = head addrinfos
  sock <- socket (addrFamily serveraddr) Datagram defaultProtocol
  setSocketOption sock Broadcast 1
  bind sock (addrAddress serveraddr)
  forever $ do
    recv sock 4096 >>= \message -> do
              let msg = C.unpack message
              let args = splitOn ";" msg
              let params = splitOn "%" (args !! 0)
              let from_ip = params !! 0
              if (params !! 0) /= ip_address
                      then do
                          case params !! 1 of
                            "FIND" -> do
                                  let name = params !! 2
                                  print name
                                  let slice from to xs = take(to - from + 1) (drop from xs)
                                  files_raw <- getCurrentDirectory >>= getDirectoryContents
                                  --print $ map lowerStr (take ((length files_raw) - 1) (drop 2 files_raw))
                                  let founded = filter (contains name) (files_raw)
                                  print founded
                                  if (length founded) > 0
                                    then do
                                      let msg = ip_address ++ "%FOUND;" ++ (intercalate ";" founded)
                                      sendPacket "5555" msg from_ip
                                    else putStr ""
                                  where
                                  lowerStr = map toLower
                                  contains f n = case n of 
                                        "." -> False
                                        ".." -> False
                                        _ -> isInfixOf (lowerStr f) (lowerStr n) 
                            "FOUND" -> do
                              let founded_files = take ((length params) - 1) (drop 2 params)
                              print founded_files
                              let msg = "FOUNDED!\n" ++ (intercalate "\n" (map (construct_msg from_ip) founded_files))
                              putStrLn(msg)
                              where
                                construct_msg from_ip file = "From " ++ from_ip ++ ": " ++ file
                              --    else putStr ""
                            _ -> putStr ""
                      else putStr("")      